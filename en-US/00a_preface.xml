<?xml version="1.0"?>
<preface id="preface">
  <title>Preface</title>

  <para>Thank you for your interest in Debian. At the time of writing,
  more than 10% of the web is powered by Debian. Think about it; how
  many web sites would you have missed today without Debian?</para>

  <para>Debian is the operating system of choice on the International
  Space Station, and countless universities, companies and public
  administrations rely on Debian to deliver services to millions of users
  around the world and beyond. Truly, Debian is a highly successful project and
  is far more pervasive in our lives than people are aware of.</para>

  <para>But Debian is much more than “just” an operating system. First,
  Debian is a concrete vision of the freedoms that people should enjoy
  in a world increasingly dependent on computers. It is forged from the
  crucible of Free Software ideals where people should be in control of
  their devices and not the other way around. With enough knowledge you
  should be able to dismantle, modify, reassemble and share the software
  that matters to you. It doesn't matter if the software is used for
  frivolous or even life-threatening tasks, you should be in control of
  it.</para>

  <para>Secondly, Debian is a very peculiar social experiment. Entirely
  volunteer-led, individual contributors take on all the
  responsibilities needed to keep Debian functioning rather than being
  delegated or assigned tasks by a company or organization. This means
  that Debian can be trusted to not be driven by the commercial
  interests or whims of companies that may not be aligned with the goal
  of promoting people's freedoms.</para>

  <para>And the book you have in your hands is vastly different from
  other books; it is a <emphasis>free as in freedom</emphasis> book, a
  book that finally lives up to Debian's standards for every aspect of
  your digital life. You can <command>apt install</command> this book,
  you can redistribute it, you can “fork” it, and even submit bug reports
  and patches so that other readers may benefit from your feedback. The maintainers of this
  book — who are also its authors — are longstanding members of the
  Debian Project who truly understand the ethos that permeate every
  aspect of the project.</para>

  <para>By writing and releasing this book, they are doing a truly
  wonderful service to the Debian community.</para>

  <para>May 2017</para>

  <para>Chris Lamb (Debian Project Leader)</para>
</preface>
